//
// Created by Laura Phillips on 7/6/16.
//


#include <mach/machine.h>
#include <string>
#include <list>

using namespace std;

#ifndef PROJECT_2_MEDIA_H
#define PROJECT_2_MEDIA_H


class Media {
    public:
        string entryID;
        string productType;
        string title;
        string description;
        string duration;
        float rentalPrice;
        string dateAvailable;
        int daysAvailable;
        bool operator=(const Media& o);

        //list<string> idList; why is this here?
        //methods Austin Raney 7/7/16
        Media();
        //Media(string productType, string title, string description, string duration, float rentalPrice, string dateAvailable, int daysAvailable);
    //I removed list as a parameter because it doesn't make sense to put it there.
        string toString();
        string toWrite();
        //string generateID(list<string> idList);
        Media(string entryID, string productType, string title, string description, string duration, float rentalPrice, string dateAvailable, int daysAvailable);
        //getters Austin Raney 7/7/16
        string getEntryID();
        string getProductType();
        string getTitle();
        string getDescription();
        string getDuration();
        float getRentalPrice();
        string getDateAvailable();
        int getDaysAvailable();

        //setters Austin Raney 7/7/16
        void setEntryID(string entryID);
        void setProductType(string productType);
        void setTitle(string title);
        void setDescription(string description);
        void setDuration(string duration);
        void setRentalPrice(float rentalPrice);
        void setDateAvailable(string dateAvailable);
        void setDaysAvailable(int daysAvailable);




        friend bool operator <(const Media& m, const Media& n);

};


#endif //PROJECT_2_MEDIA_H

