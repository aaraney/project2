Implement a streaming data servicer management program can be used as a prototype to schedule the availability of media for a streaming data service provider. 
Your program will allow the scheduler (or user) to enter the properties associated with various types of streaming media (e.g. movies, music, etc.). The program will manage a list of properties for each streaming data product as a node within a single linked list stored in memory (this can be a global variable for simplicity).

The program must use the “list” API in the C++ standard template library (STL).

The program must implement at least one class that will hold the following variables (input validation is only
required where indicated):

*  `entryID` — A string variable that will hold an automatically generated 10-character string as the entry identifier. The string must be in the form LLL-NNNLL,
where L is a character from A to Z, and N is a number from 0 to
9. For example: HEJ-268-UY. The entryID is a unique identifier for
each streaming product and repeats are not allowed.
 
* `productType` — A string variable that indicate the type of streaming content. Valid content items are: `movie, music, television, news, and radio`.
**Input Validation Required:** The program must re-prompt the user if the
entered product type does not match one of the valid content items.

* `title` – A string variable to hold the title of the streaming entry. Note that
spaces between words must be allowed so that all of this information
can be entered.

* `description` - A string variable to hold a brief description of the streaming entry. Again that spaces between words must be allowed so that all of this
information can be entered.

* `durationTime` - A string variable to hold the playing time for the streaming data
product. The provided time must be in the form `hh:mm:ss`, where hh
is the number of hours (00 - 12) and mm is the number of minutes (00 – 60), and ss is the number of second (00 – 60).
**Input Validation Required:** The program must re-prompt the user if the entered duration time does not match the above format.

* `rentalPrice` — A float variable to hold the price of the streaming product.

* `dateAvailable` - A string variable to hold the date that the media will be available. The entered date will be in the form `mm/dd/yyyy`.

* `daysAvailable` - An integer variable to hold the number of days that the media will be available.

Provide the appropriate methods to set and get the data for each of these class variables. 
For example `setTitle(string title)` and `string getTitle()`. The BookInventory provides an example on how this can be done. In addition, the main program must provide the following functionality:

1. When the program is first started, it must read a data file called media_list.dat. The program will not prompt the user for the name of the data file. The name of the file must be hard-coded in the program. 
If the file exists, the program will load the data for each media type into the global linked list. If the file does not exist, the program will start with an empty linked list.

2. The program will provide a simple text-based user interface that manages the all of the schedule media within a linked list. 
Each entry must be placed in linked list as an object that holds all of the information associated with it as mentioned above. The user interface will allow the user to perform the following:

**(a)** Display Content – displays all of the scheduled media within the linked list along with their associated attributes. In addition, this option must print the total number of scheduled entries within the linked list and the total rental prices for all of the scheduled.

**(b)** Enter Media Item – allows the user to enter all of the fields associated with a given media item, except for the `entryID`, which will be automatically generated by the program as previously mentioned. 
After the fields are entered, the program will place the media object into the global linked list.

**(c)** Delete Media Item – allows the user to delete a media item from the linked list using the `entryID` as the key. 
The program must display a message if the provided `entryID` is not found in the linked list.

**(d)** Search for Media Item – allows the user to find a media item. The program will prompt the user to enter the `entryID` and will display all of the fields associated with the given media item if it is found. 
The program must display a message if the item is not found in the linked list.

**(e)** Edit Media Item – allows the user to edit the fields for a given media item 
that is in the linked list (except for the `entryID`). 
The program must prompt the user to enter the entryID as the key to find the item to edit. Print a message if the media item is not found in the linked list. For
simplicity, the program may re-prompt the user to re-enter all of the fields associated with the given media item; but again, it must reuse the `entryID` value.

**(f)** Exit System – before the program exits, it must save all of the data in the linked list to the data file. I recommend using a standard text file with one field per line. At this point, if the file does not exist, the program will create it.