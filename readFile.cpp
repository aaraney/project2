//
// Created by Laura Phillips on 7/10/16.
//
#include <fstream>
#include <iostream>
#include "readFile.h"


list<Media> readFile(MediaList ml){
    ifstream file("media_list.dat");
    string line, entryID, title, type, description, duration, date;
    float price = 0;
    int days = 0;
    int i = 1;

    while (getline(file, line)) {

        if (line.length() == 0)continue;

        switch (i % 7) {
            case 1:
                entryID = ml.generateID();
                type = line;
                break;
            case 2:
                title = line;
                break;
            case 3:
                description = line;
                break;
            case 4:
                duration = line;
                break;
            case 5:
                price = stoi(line);
                break;
            case 6:
                date = line;
                break;
            default:
                days = stoi(line);

                ml.mediaList.push_back(Media(entryID,type,title,description,duration,price,date,days));
        }
        i++;
    }
    return ml.mediaList;
}

