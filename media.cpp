//
// Created by Laura Phillips on 7/6/16.
//
#include <stdlib.h>
#include "media.h"
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>


Media::Media() {

}
Media::Media (string entryID, string productType, string title, string description, string duration, float rentalPrice, string dateAvailable, int daysAvailable) {
    this->entryID = entryID;
    this->productType = productType;
    this->title = title;
    this->description = description;
    this->duration = duration;
    this->rentalPrice = rentalPrice;
    this->dateAvailable = dateAvailable;
    this->daysAvailable= daysAvailable;
}

string Media::toString(){ //Written by Austin Raney 7/7/16
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << rentalPrice;
    std::string formattedPrice = ss.str();

    string printable = "ID: " + entryID +
            "\nType: " + productType +
            "\nTitle: " + title +
            "\nDescription: " + description +
            "\nDuration: " + duration +
            "\nRental Price: $" + formattedPrice  +
            "\nDate Available: " + dateAvailable +
            "\nDay(s) Available: " + to_string(daysAvailable)+ "\n\n";
    return printable;
}

string Media::toWrite(){
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << rentalPrice;
    std::string formattedPrice = ss.str();

    string printable =  entryID
            + "\n" + productType
            + "\n" + title
            + "\n" + description 
            + "\n" + duration 
            + "\n" + formattedPrice
            + "\n" + dateAvailable
            + "\n" + to_string(daysAvailable) + "\n";
    return printable;
}

//Written by Austin Raney 7/7/16 getters

string Media::getEntryID(){
    return entryID;
}

string Media::getProductType(){
    return productType;
}

string Media::getTitle(){
    return title;
}

string Media::getDescription(){
    return description;
}

string Media::getDuration(){
    return duration;
}

float Media::getRentalPrice(){
    return rentalPrice;
}

string Media::getDateAvailable(){
    return dateAvailable;
}

int Media::getDaysAvailable(){
    return daysAvailable;
}

//Written by Austin Raney 7/7/16 setters
void Media::setEntryID(string entryID){
    this->entryID = entryID;
}

void Media::setProductType(string productType){
    this->productType = productType;
}

void Media::setTitle(string title){
    this->title = title;
}

void Media::setDescription(string description){
    this->description = description;
}

void Media::setDuration(string duration){
    this->duration = duration;
}

void Media::setRentalPrice(float rentalPrice){
    this->rentalPrice = rentalPrice;
}

void Media::setDateAvailable(string dateAvailable){
    this->dateAvailable = dateAvailable;
}

void Media::setDaysAvailable(int daysAvailable){
    this->daysAvailable = daysAvailable;
}

bool operator<(const Media& m, const Media& n){
    return tie(m.entryID, m.productType, m.title, m.description, m.duration, m.rentalPrice, m.dateAvailable, m.daysAvailable) < tie(n.entryID, n.productType, n.title, n.description, n.duration, n.rentalPrice, n.dateAvailable, n.daysAvailable);
}
bool Media::operator =(const Media& o){
    return entryID == o.entryID;
}