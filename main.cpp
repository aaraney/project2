#include <iostream>
#include <fstream>
#include <list>

#include "readFile.h"
#include "media.h"
#include <iomanip>


using namespace std;

#include <iomanip>
#include <sys/stat.h>
#include "MediaList.h"


MediaList mediaList;

void menu(){
    cout << "Please choose an action by entering a number 1 through 6." << endl;
    cout << "1. Display Content" << endl;
    cout << "2. Enter Media Item" << endl;
    cout << "3. Delete Media Item" << endl;
    cout << "4. Search for Media Item" << endl;
    cout << "5. Edit Media Item" << endl;
    cout << "6. Exit Program" << endl;
}

bool isFile(const std::string& filename) {
    struct stat buffer;
    return (stat (filename.c_str(), &buffer) == 0);
}

void displayMenu() {
    //implment readFile function
    if(isFile("media_list.dat")){
        mediaList.mediaList.merge(readFile(mediaList));
    }

    int choice;
    menu();
    string r,s,e;
    while(1){

        cin >> choice;

        switch (choice){
            case 1: //display
                mediaList.printList();
                break;
            case 2: //enter new media item
                mediaList.add();
                break;
            case 3: //delete
                cout << "Enter the ID of the item that you would like to remove from the list." << endl;
                cin >> r;
                mediaList.remove(r);
                break;
            case 4: //search list
                cout << "Enter the ID of the item that you would like to find." << endl;
                cin >> s;
                mediaList.search(s);
                break;
            case 5: //edit
                cout << "Enter the ID of the item that you would like to edit." << endl;
                cin >> e;
                mediaList.edit(e);
                break;
            case 6: //exit
                exit(1);
                break;
            default:
                cout << "Invalid option please try again." << endl;
                cin.clear();
                cin.ignore(INT_MAX,'\n');
                //something here
        }
        cout << "\n\n";
        //cin.clear();
        //cin.ignore(INT_MAX,'\n');
        menu(); //I think this is the right spot?
    }
}


int main(int argc, char* argv[])
{
    displayMenu();
}