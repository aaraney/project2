//
// Created by Austin Raney on 7/15/16.
//

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <regex>
#include "MediaList.h"

void MediaList::cleanCin(){
    cin.clear();
    cin.ignore(INT_MAX,'\n');
}

void MediaList::add() { //used for enter media
    Media m;
    string entry;
    double price;
    int days;
    bool correctFormat = false;
    m.setEntryID(generateID()); //set entryID with unique ID
    while(!correctFormat) {
        cout << "Enter media type: ";
        cin >> entry;
        transform(entry.begin(), entry.end(), entry.begin(), ::tolower); //may need to impliment a helper method
        correctFormat = formatMatch("media", entry);
        if(correctFormat != 1){
            cout << "Please enter one of the following: music, movie, television, news, radio\n";
            cleanCin();
        }
    }
    m.setProductType(entry);
    cleanCin();
    correctFormat = false;

    cout << "Enter title: ";
    cin >> entry;
    m.setTitle(entry);
    cleanCin();

    cout << "Enter description: ";
    cin >> entry;
    m.setDescription(entry);
    cleanCin();

    while(!correctFormat) {
        cout << "Enter duration: ";
        cin >> entry;
        correctFormat = formatMatch("time", entry);
        if(correctFormat != 1){
            cout << "Please enter the time using the format hh:mm:ss\n";
            cleanCin();
        }
    }
    m.setDuration(entry);
    cleanCin();
    correctFormat = false;

    cout << "Enter rental price: ";
    while(!(cin >> price)){
        cout << "Bad value! Use the format #.## \nEnter new rental price: ";
        cleanCin();
    }
    m.setRentalPrice(price);
    cleanCin();


    while(!correctFormat) {
        cout << "Enter date available: ";
        cin >> entry;
        correctFormat = formatMatch("date", entry);
        if(correctFormat != 1){
            cout << "Please enter the date using the format mm/dd/yyyy\n";
            cleanCin();
        }
    }
    m.setDateAvailable(entry);
    cleanCin();

    cout << "Enter days available: ";
    while(!(cin >> days) || days < 0){
        cout << "Bad value! Must be a positive integer.\nEnter new days available: ";
        cleanCin();
    }
    m.setDaysAvailable(days);
    cleanCin();

    mediaList.push_back(m); //push ont end of list
    cout << m.title + " was successfully added.\n";
}

Media MediaList::find(string id) { //11:20 new find version; tested functionality in other project
    std::list<Media>::iterator itr = mediaList.begin();
    while (itr != mediaList.end()) {
        if (itr->entryID.compare(id) == 0) {
            return *itr;
        }
        itr++;
    }
}

void MediaList::remove(string s) { //s is the entryID
    bool inList = false;
    std::list<Media>::iterator itr = mediaList.begin();
    while (itr != mediaList.end()) {
        if (itr->entryID.compare(s) == 0) {
            inList = true;
            itr = mediaList.erase(itr);
        }
        itr++;
    }
    if(!inList){
        cout << s + " is not in the list. Use option 1 to view the list.\n";
    }
}

bool MediaList::inList(string s) { //s is the entryID
    std::list<Media>::iterator itr = mediaList.begin(); //added new itr 11:23 reference
    while(itr != mediaList.end()){
        if((*itr).entryID.compare(s) == 0)
            return true;
        itr++;
    }
    return false;
}

//Laura Phillips 7/11/16
string MediaList::generateID() { //
    string id;
    for(int i = 0; i < 3; i++) {
        char letter = 'A' + rand()%26;
        id+= letter;
    }
    id += "-";
    for(int i = 0; i < 2; i++) {
        int num = rand() % 10;
        string strNum = to_string(num);
        id+= strNum;
    }
    id += "-";
    for(int i = 0; i < 3; i++) {
        char letter = 'A' + rand() % 26;
        id += letter;
    }
    if (inList(id)) {
        generateID();
    }
    else {
        return id;
    }
}

void MediaList::printList() {
    float totalPrice = 0;
    for (std::list<Media>::iterator it=mediaList.begin(); it != mediaList.end(); ++it) {
        totalPrice += (*it).getRentalPrice();
        cout << (*it).toString();
    }

    cout << "Total Number of Scheduled Entries: " + to_string(mediaList.size()) + "\n";
    cout << "Total Rental Prices of Scheduled Entries: " << fixed << setprecision(2) << totalPrice << endl;
    cout << '\n';
}

//FUNC NEEDS TO BE EDITED. CHANGED 11:25
void MediaList::search(string s) { //s is entryID of element to be found
    bool inList = false;
    std::list<Media>::iterator itr = mediaList.begin();
    while (itr != mediaList.end()) {
        if (itr->entryID.compare(s) == 0) {
            inList = true;
            cout << (*itr).toString();
        }
        itr++;
    }
    if(!inList){
        cout << s + " is not in the list. Use option 1 to view the list.\n";
    }
}

void MediaList:: edit(string s) { //s is Entry ID
    string entry;
    double price;
    int days;
    bool correctFormat = false;
    bool inList = false;
    std::list<Media>::iterator itr = mediaList.begin();
    while (itr != mediaList.end()) {
        if (itr->entryID.compare(s) == 0) {
            inList = true;
            while (!correctFormat) {
                cout << "Enter new media type: ";
                cin >> entry;
                transform(entry.begin(), entry.end(), entry.begin(), ::tolower); //may need to impliment a helper method
                correctFormat = formatMatch("media", entry);
                if (correctFormat != 1) {
                    cout << "Please enter one of the following: music, movie, television, news, radio\n";
                    cleanCin();
                }
            }
            (*itr).setProductType(entry);
            cleanCin();
            correctFormat = false;

            cout << "Enter new title: ";
            cin >> entry;
            (*itr).setTitle(entry);
            cleanCin();

            cout << "Enter new description: ";
            cin >> entry;
            (*itr).setDescription(entry);
            cleanCin();

            while (!correctFormat) {
                cout << "Enter new duration: ";
                cin >> entry;
                correctFormat = formatMatch("time", entry);
                if (correctFormat != 1) {
                    cout << "Please enter the time using the format hh:mm:ss\n";
                    cleanCin();
                }
            }
            (*itr).setDuration(entry);
            cleanCin();
            correctFormat = false;

            cout << "Enter new rental price: ";
            while (!(cin >> price)) {
                cout << "Bad value! Use the format #.## \nEnter new rental price: ";
                cleanCin();
            }
            (*itr).setRentalPrice(price);
            cleanCin();
            while (!correctFormat) {
                cout << "Enter new date available: ";
                cin >> entry;
                correctFormat = formatMatch("date", entry);
                if (correctFormat != 1) {
                    cout << "Please enter the date using the format mm/dd/yyyy\n";
                    cleanCin();
                }
            }
            (*itr).setDateAvailable(entry);
            cleanCin();

            cout << "Enter new days available: ";
            while (!(cin >> days) || days < 0) {
                cout << "Bad value! Must be a positive integer.\nEnter new days available: ";
                cleanCin();
            }
            (*itr).setDaysAvailable(days);
            cout << (*itr).title + " was successfully updated!\n";
            cleanCin();
        }
        itr++;
    }
    if (!inList) {
        cout << s + " is not in the list. Use option 1 to view the list.\n";
    }
}

bool MediaList::formatMatch(string type, string compare) {  //input validation
    if (type.compare("media") == 0) {
        transform(compare.begin(), compare.end(), compare.begin(), ::tolower);
        return compare == "music" || compare == "movie" || compare == "television" || compare == "news" || compare == "radio"; //changed 6:46
    }
    if (type.compare("time") == 0) {
        std::regex timeformat("^([0-1]?\\d|2[0-3]):([0-5]?\\d):([0-5][0-9])$");
        return std::regex_match(compare, timeformat);

        //try {
        //    if(compare.size() != 8)return false;
        //    int hour = stoi(compare[0:1]);
        //    int sec = stoi(compare[3:4]);
        //    int min = stoi(compare[6:7]);
        //    char delimiter = ':';
        //    string compareFull = compare;
        //    size_t pos = 0;
        //    string token;
        //    string tokens[] = {};
        //    int i = 0;
        //    while ((pos = compare.find(delimiter)) != string::npos) {
        //        token = compare.substr(0, pos);
        //        tokens[i] = token;
        //        compare.erase(0, pos + 1);
        //        i++;
        //    }
        //    tokens[2] = compare;
        //    int hours = stoi(tokens[0]);
        //    int minutes = stoi(tokens[1]);
        //    int seconds = stoi(tokens[2]);
        //    else if (hours =< 0 || hours > 60 || minutes =< 0 || minutes > 60 || seconds =< 0 || seconds > 60) {
        //        return false;
        //    }
//
        //    else {
        //        return true;
        //    }
        //}
        //catch (...) {
        //    return false;
        //}
    }
    if(type.compare("date") == 0){
        std::regex timeformat("^(0[1-9]|1[012])\\/(0[1-9]|[12][0-9]|3[01])\\/\\d\\d\\d\\d");
        return std::regex_match(compare, timeformat);
    }
}