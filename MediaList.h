//
// Created by Austin Raney on 7/15/16.
//
#include <list>
#include "media.h"
#include <map>


using namespace std;

#ifndef PROJECT2_MEDIALIST_H
#define PROJECT2_MEDIALIST_H


class MediaList {
public:
    list<Media> mediaList; //list of all content
    list<Media>::iterator it = mediaList.begin(); //iterator for pointer list

    void add(); //insert item into the list
    void remove(string s); //remove media item from the list
    bool inList(string s); //return if item is in list or not.
    Media find(string id); //return if item is in list or not.
    string generateID();
    void printList();
    void search(string s);
    void edit(string s);
    bool formatMatch(string type, string compare);
    void cleanCin();

};


#endif //PROJECT2_MEDIALIST_H
